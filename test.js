// ş
'use strict';

var EOL = require('os').EOL;

try {
    var csslintCfg = JSON.stringify(require('./main'), null, '  ');
    
    console.info(`.csslintrc looks good:${EOL}${EOL}${csslintCfg}`);
}
catch (ex) {
    console.error("There's something wrong with the .csslintrc file!");
    throw ex;
}
